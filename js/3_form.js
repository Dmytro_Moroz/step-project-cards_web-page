import Request from "./1_ajax-requests.js";
import Visit, {VisitCardiologist, VisitDentist, VisitTherapist} from "./4_visits.js";
import LiveSearch from "./6_live-search.js";

export default class Form {
    constructor(typeOfForm, visit, id, data) {
        // visit - это непосредственно сам объект visit.
        this._el = document.createElement('form');

        this._visit = visit;
        this._id = id;
        this._data = data;

        if (typeOfForm === "login") {
            this._el.classList.add("login-form");
            this._loginIcon = document.createElement("img");
            this._loginIcon.src = "./img/modal/login-icon.svg";
            this._loginIcon.classList.add('login-icon');
            this._loginIcon.setAttribute("alt", "login icon");
            this._el.append(this._loginIcon);
            // -----------------------------
            this._notification = document.createElement("p");
            this._notification.classList.add('notification');
            this._notification.innerText = "Wrong e-mail or password was entered! Please try again.";
            this._notification.style.visibility = 'hidden';
            this._notification.style.opacity = "0";
            this._el.append(this._notification);
            // -----------------------------
            this._emailInput = new Input("email");
            this._el.append(this._emailInput);
            // -----------------------------
            this._passwordInput = new Input("password");
            this._el.append(this._passwordInput);
            // -----------------------------
            this._loginBtn = document.createElement('button');
            this._loginBtn.setAttribute('type', 'submit');
            this._loginBtn.innerText = "LOGIN";
            this._loginBtn.classList.add('login-btn');
            this._el.append(this._loginBtn);

            // Event handler for login form goes below.
            this._el.addEventListener('submit', function (e) {
                e.preventDefault();

                let formData = new FormData(this);
                formData = Object.fromEntries(formData);

                new Request("login", formData, null)
                    .then((response) => {
                        return response.json();
                    })
                    .then((data) => {
                        // console.log(data);
                        if (data.status === "Success") {
                            sessionStorage.setItem('token', data.token);
                            // сохраняем полученный токен в sessionStorage. Это нужно нам для того, чтобы в дальнейшем при каждом запросе на сервер (вид запроса не важен) в секцию headers добавлять этот токен, чтобы сервер мог аутентифицировать пользователя.
                            Form.onLoginAnimation();
                            // анимация (спиннер) после успешного входа в систему.

                            new Promise((resolve, reject) => {
                                resolve(new Request("getAllVisits"));
                            }).then((data) => {
                                // console.log(data);
                                if (data.length !== 0) {
                                    Visit.renderAllVisits(data);
                                }
                            })
                            LiveSearch.filterShow();
                        } else if (data.status === "Error") {
                            let notification = document.querySelector(".notification");
                            notification.style.visibility = 'visible';
                            notification.style.opacity = "1";
                        }
                        return data;
                    })
                    .catch(error => console.error(error));

                this.reset();
            })

            return this._el;
        }

        if (typeOfForm === "createVisit") {
            this._el.classList.add('create-visit-form');

            this._heading = document.createElement('h2');
            this._heading.classList.add("h2");
            this._heading.innerText = "Please fill in data about new visit";
            this._el.append(this._heading);

            this._mainSelect = new Select("doctorSelect");
            this._el.append(this._mainSelect);

            return this._el;
        }

        if (typeOfForm === "editVisit") {
            this._el.classList.add('edit-visit-form');

            this._heading = document.createElement('h2');
            this._heading.classList.add("h2");
            this._heading.innerText = "Please edit information about this visit:";
            this._el.append(this._heading);

            if (this._data.doctor === "Cardiologist") {
                this._doctor = new InputFieldTitle("Doctor: ");
                this._doctor.insertAdjacentHTML("beforeend", `${this._data.doctor}`);
                this._urgency = new InputFieldTitle("Urgency: ");
                this._urgency.insertAdjacentHTML("beforeend", `${this._data.urgency}`);
                // ------------------------------------------
                this._fullName = new Input("fullName");
                this._fullName.value = data.fullName;
                this._age = new Input("age");
                this._age.value = data.age;
                this._inputPurpose = new Input("purposeOfVisit");
                this._inputPurpose.value = data.purposeOfVisit;
                this._bloodPressure = new Input('bloodPressure');
                this._bloodPressure.value = data.bloodPressure;
                this._bodyMassIndex = new Input("bodyMassIndex");
                this._bodyMassIndex.value = data.bodyMassIndex;
                this._pastIllnesses = new Textarea("pastIllnesses");
                this._pastIllnesses.value = data.pastIllnesses;

                this._shortDescription = new Textarea("shortDescription");
                this._shortDescription.value = data.shortDescription;
                // ---------------------------------------
                this._editVisitModalBtn = new Button("editVisitModalBtn");

                this._onEditVisitClickBound = this.onEditVisitClick.bind(this);
                this._el.addEventListener('submit', this._onEditVisitClickBound);
                // ---------------------------------------
                this._el.append(this._doctor);
                this._el.append(this._urgency);
                this._el.append(new InputFieldTitle("Full name:"));
                this._el.append(this._fullName);
                this._el.append(new InputFieldTitle("Age:"));
                this._el.append(this._age);
                this._el.append(new InputFieldTitle("Purpose of visit:"));
                this._el.append(this._inputPurpose);
                this._el.append(new InputFieldTitle("Usual blood pressure:"));
                this._el.append(this._bloodPressure);
                this._el.append(new InputFieldTitle("Body mass index:"));
                this._el.append(this._bodyMassIndex);
                this._el.append(new InputFieldTitle("Past illnesses of cardiovascular system:"));
                this._el.append(this._pastIllnesses);
                this._el.append(new InputFieldTitle("Short description:"));
                this._el.append(this._shortDescription);
                this._el.append(this._editVisitModalBtn);
            }

            if (this._data.doctor === "Dentist") {
                this._doctor = new InputFieldTitle("Doctor: ");
                this._doctor.insertAdjacentHTML("beforeend", `${this._data.doctor}`);
                this._urgency = new InputFieldTitle("Urgency: ");
                this._urgency.insertAdjacentHTML("beforeend", `${this._data.urgency}`);
                // ------------------------------------------
                this._fullName = new Input("fullName");
                this._fullName.value = data.fullName;
                this._inputPurpose = new Input("purposeOfVisit");
                this._inputPurpose.value = data.purposeOfVisit;
                this._inputDateOfPrevVisit = new Input("dateOfPreviousVisit");
                this._inputDateOfPrevVisit.value = data.dateOfPreviousVisit;
                this._shortDescription = new Textarea("shortDescription");
                this._shortDescription.value = data.shortDescription;
                // ---------------------------------------
                this._editVisitModalBtn = new Button("editVisitModalBtn");

                this._onEditVisitClickBound = this.onEditVisitClick.bind(this);
                this._el.addEventListener('submit', this._onEditVisitClickBound);
                // // ---------------------------------------
                this._el.append(this._doctor);
                this._el.append(this._urgency);
                this._el.append(new InputFieldTitle("Full name:"));
                this._el.append(this._fullName);
                this._el.append(new InputFieldTitle("Purpose of visit:"));
                this._el.append(this._inputPurpose);
                this._el.append(new InputFieldTitle("Date of previous visit:"));
                this._el.append(this._inputDateOfPrevVisit);
                this._el.append(new InputFieldTitle("Short description:"));
                this._el.append(this._shortDescription);
                this._el.append(this._editVisitModalBtn);
            }

            if (this._data.doctor === "Therapist") {
                this._doctor = new InputFieldTitle("Doctor: ");
                this._doctor.insertAdjacentHTML("beforeend", `${this._data.doctor}`);
                this._urgency = new InputFieldTitle("Urgency: ");
                this._urgency.insertAdjacentHTML("beforeend", `${this._data.urgency}`);
                // ------------------------------------------
                this._fullName = new Input("fullName");
                this._fullName.value = data.fullName;
                this._age = new Input("age");
                this._age.value = data.age;
                this._inputPurpose = new Input("purposeOfVisit");
                this._inputPurpose.value = data.purposeOfVisit;
                this._shortDescription = new Textarea("shortDescription");
                this._shortDescription.value = data.shortDescription;
                // ---------------------------------------
                this._editVisitModalBtn = new Button("editVisitModalBtn");

                this._onEditVisitClickBound = this.onEditVisitClick.bind(this);
                this._el.addEventListener('submit', this._onEditVisitClickBound);
                // // ---------------------------------------
                this._el.append(this._doctor);
                this._el.append(this._urgency);
                this._el.append(new InputFieldTitle("Full name:"));
                this._el.append(this._fullName);
                this._el.append(new InputFieldTitle("Age:"));
                this._el.append(this._age);
                this._el.append(new InputFieldTitle("Purpose of visit:"));
                this._el.append(this._inputPurpose);
                this._el.append(new InputFieldTitle("Short description:"));
                this._el.append(this._shortDescription);
                this._el.append(this._editVisitModalBtn);
            }
            return this._el;
        }

        if (typeOfForm === "extraInputsCardio") {
            this._el = document.querySelector(".create-visit-form");

            this._fullName = new Input("fullName");
            this._age = new Input("age");
            this._inputPurpose = new Input("purposeOfVisit");
            this._bloodPressure = new Input('bloodPressure');
            this._bodyMassIndex = new Input("bodyMassIndex");
            this._pastIllnesses = new Textarea("pastIllnesses");
            this._urgency = new Select("urgency");
            this._shortDescription = new Textarea("shortDescription");
            // ---------------------------------------
            this._createVisitModalBtn = new Button("createVisitModalBtn");

            this._onCreateVisitClickBound = this.onCreateVisitClick.bind(this);
            this._el.addEventListener('submit', this._onCreateVisitClickBound);
            // ---------------------------------------
            this._el.append(this._fullName);
            this._el.append(this._age);
            this._el.append(this._inputPurpose);
            this._el.append(this._bloodPressure);
            this._el.append(this._bodyMassIndex);
            this._el.append(this._pastIllnesses);
            this._el.append(this._urgency);
            this._el.append(this._shortDescription);
            this._el.append(this._createVisitModalBtn);
        }

        if (typeOfForm === "extraInputsDentist") {
            this._el = document.querySelector(".create-visit-form");

            this._fullName = new Input("fullName");
            this._inputPurpose = new Input("purposeOfVisit");
            this._urgency = new Select("urgency");
            this._dateOfPreviousVisit = new Input("dateOfPreviousVisit")
            this._shortDescription = new Textarea("shortDescription");
            // ---------------------------------------
            this._createVisitModalBtn = new Button("createVisitModalBtn");

            this._onCreateVisitClickBound = this.onCreateVisitClick.bind(this);
            this._el.addEventListener('submit', this._onCreateVisitClickBound);
            // ---------------------------------------
            this._el.append(this._fullName);
            this._el.append(this._inputPurpose);
            this._el.append(this._urgency);
            this._el.append(this._dateOfPreviousVisit);
            this._el.append(this._shortDescription);
            this._el.append(this._createVisitModalBtn);
        }

        if (typeOfForm === "extraInputsTherapist") {
            this._el = document.querySelector(".create-visit-form");

            this._fullName = new Input("fullName");
            this._age = new Input("age");
            this._inputPurpose = new Input("purposeOfVisit");
            this._urgency = new Select("urgency");
            this._shortDescription = new Textarea("shortDescription");
            this._createVisitModalBtn = new Button("createVisitModalBtn");

            this._onCreateVisitClickBound = this.onCreateVisitClick.bind(this);
            this._el.addEventListener('submit', this._onCreateVisitClickBound);
            // ---------------------------------------
            this._el.append(this._fullName);
            this._el.append(this._age);
            this._el.append(this._inputPurpose);
            this._el.append(this._urgency);
            this._el.append(this._shortDescription);
            this._el.append(this._createVisitModalBtn);
        }

    }

    static onLoginAnimation() {
        let popUpContainer = document.querySelector(".popup");
        popUpContainer.innerHTML = `<div class="lds-ring"><div></div><div></div><div></div><div></div></div>`;
        setTimeout(() => {
            let signInBtn = document.querySelector('.sign-in-btn');
            signInBtn.style.display = "none";
            let createVisitBtn = document.querySelector(".create-visit-btn");
            createVisitBtn.style.display = "inline-block";
            popUpContainer.remove();
        }, 1200);
    }

    static onAnyVisitActionAnimation() {
        let popUpContainer = document.querySelector(".popup");
        popUpContainer.innerHTML = `<div class="lds-ring"><div></div><div></div><div></div><div></div></div>`;
        setTimeout(() => {
            popUpContainer.remove();
        }, 1200);
    }

    onCreateVisitClick(e) {
        e.preventDefault();

        let selectedDoctor = document.querySelector(".doctor-select").value;
        let selectedUrgency = document.querySelector(".urgency-select").value;

        let formData = new FormData(this._el);
        formData = Object.fromEntries(formData);
        formData["doctor"] = selectedDoctor;
        formData["urgency"] = selectedUrgency;
        // console.log(formData);

        new Request("post", formData, null)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                Form.onAnyVisitActionAnimation();
                // spinner animation на время создания визита.
                let noVisitsNotice = document.querySelector(".no-visits-notice");
                if (noVisitsNotice) {
                    noVisitsNotice.remove()
                }

                switch (data.doctor) {
                    case "Cardiologist":
                        new VisitCardiologist(data);
                        break;
                    case "Dentist":
                        new VisitDentist(data);
                        break;
                    case "Therapist":
                        new VisitTherapist(data);
                        break;
                }
            })
            .catch(error => console.error(error));

        this._el.reset();
    }

    onEditVisitClick(e) {
        e.preventDefault();

        let formData = new FormData(this._el);
        formData = Object.fromEntries(formData);
        formData["doctor"] = this._data.doctor;
        formData["urgency"] = this._data.urgency;

        new Request("put", formData, this._id)
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this._visit.updateValue(data, this._visit);
                Form.onAnyVisitActionAnimation();
            })
            .catch(error => console.error(error));
    }
}

export class Input {
    constructor(typeOfInput) {
        this._el = document.createElement('input');
        if (typeOfInput === "email") {
            this._el.setAttribute('type', 'text');
            this._el.setAttribute('name', 'email');
            this._el.classList.add('email-input');
            this._el.setAttribute("required", "required");
            this._inputLabel = document.createElement("label");
            this._inputLabel.classList.add("input-label");
            this._inputLabel.innerText = "E-mail";
            this._inputLabel.append(this._el);
            return this._inputLabel;
        }

        if (typeOfInput === "password") {
            this._el.setAttribute('type', 'password');
            this._el.setAttribute('name', 'password');
            this._el.classList.add('password-input');
            this._el.setAttribute("required", "required");
            this._inputLabel = document.createElement("label");
            this._inputLabel.classList.add("input-label");
            this._inputLabel.innerText = "Password";
            this._inputLabel.append(this._el);
            return this._inputLabel;
        }

        if (typeOfInput === "fullName") {
            this._el.setAttribute('type', 'text');
            this._el.setAttribute('name', 'fullName');
            this._el.classList.add("one-line-input");
            this._el.setAttribute("required", "required");
            this._el.setAttribute('placeholder', "Full name");
            return this._el;
        }

        if (typeOfInput === "age") {
            this._el.setAttribute('type', 'number');
            this._el.setAttribute('name', 'age');
            this._el.classList.add("one-line-input");
            this._el.setAttribute("required", "required");
            this._el.setAttribute('placeholder', "Age");

            this.checkAgeInputBound = this.checkAgeInput.bind(this);
            this._el.addEventListener("focusout", this.checkAgeInputBound);

            return this._el;
        }

        if (typeOfInput === "shortDescription") {
            this._el.setAttribute('type', 'text');
            this._el.setAttribute('name', 'shortDescription');
            this._el.classList.add("one-line-input");
            this._el.setAttribute("required", "required");
            this._el.setAttribute('placeholder', "Short description of visit");
            return this._el;
        }

        if (typeOfInput === "purposeOfVisit") {
            this._el.setAttribute('type', 'text');
            this._el.setAttribute('name', 'purposeOfVisit');
            this._el.classList.add("one-line-input");
            this._el.setAttribute("required", "required");
            this._el.setAttribute('placeholder', "Purpose of visit");
            return this._el;
        }

        if (typeOfInput === "bloodPressure") {
            this._el.setAttribute('type', 'text');
            this._el.setAttribute('name', 'bloodPressure');
            this._el.classList.add("one-line-input");
            this._el.setAttribute("required", "required");
            this._el.setAttribute('placeholder', "Usual blood pressure");
            return this._el;
        }

        if (typeOfInput === "bodyMassIndex") {
            this._el.setAttribute('type', 'text');
            this._el.setAttribute('name', 'bodyMassIndex');
            this._el.classList.add("one-line-input");
            this._el.setAttribute("required", "required");
            this._el.setAttribute('placeholder', "Body mass index");
            return this._el;
        }
        if (typeOfInput === "dateOfPreviousVisit") {
            this._el.setAttribute('type', 'text');
            this._el.setAttribute('name', 'dateOfPreviousVisit');
            this._el.classList.add("one-line-input");
            this._el.setAttribute("required", "required");
            this._el.setAttribute('placeholder', "Date of previous visit");
            return this._el;
        }
    }

    checkAgeInput() {
        if (this._el.value<5 || this._el.value>100) {
            alert(`You have entered age ${this._el.value}. Please enter the age of patient in the range from 5 to 100 years.`);
            this._el.value = "";
        }
    }
}

export class Select {
    constructor(typeOfSelect) {
        if (typeOfSelect === "doctorSelect") {
            this._el = document.createElement("select");
            this._el.classList.add("doctor-select");
            this._el.setAttribute("required", "required");
            this._el.insertAdjacentHTML("afterbegin",
                `<option selected disabled value=""> -- Select doctor -- </option>
                 <option>Cardiologist</option>
                 <option>Dentist</option>
                 <option>Therapist</option>
            `)
            this._onDoctorSelectBound = this._onDoctorSelect.bind(this);
            this._el.addEventListener("change", this._onDoctorSelectBound);

            return this._el;
        }

        if (typeOfSelect === "urgency") {
            this._select = document.createElement("select");
            this._select.classList.add("urgency-select");
            this._select.setAttribute("required", "required");
            this._select.insertAdjacentHTML("afterbegin",
                `<option selected disabled value=""> -- Select urgency -- </option>
                 <option>Regular</option>
                 <option>Priority</option>
                 <option>Urgent</option>
            `)
            return this._select;
        }
    }

    _onDoctorSelect() {
        let selectedIndex = document.querySelector(".doctor-select").selectedIndex;
        let options = document.querySelector(".doctor-select").options;
        let selectedOption = options[selectedIndex].value;
        // console.log(selectedOption);

        let createVisitForm = document.querySelector('.create-visit-form');
        while (createVisitForm.children.length > 2) {
            createVisitForm.removeChild(createVisitForm.lastChild);
        }
        // Каждый раз при выборе в форме другого врача мы очищаем всю форму, кроме select drop-down input для выбора врача.

        if (selectedOption === "Cardiologist") {
            new Form("extraInputsCardio");
        }
        if (selectedOption === "Dentist") {
            new Form("extraInputsDentist");
        }
        if (selectedOption === "Therapist") {
            new Form("extraInputsTherapist");
        }
    }
}

export class Textarea {
    constructor(typeOfTextarea) {
        this._el = document.createElement('textarea');
        if (typeOfTextarea === "shortDescription") {
            this._el.setAttribute('name', 'shortDescription');
            this._el.classList.add("textarea");
            this._el.setAttribute('placeholder', "Short description of visit");
            return this._el;
        }
        if (typeOfTextarea === "pastIllnesses") {
            this._el.setAttribute('name', 'pastIllnesses');
            this._el.classList.add("textarea");
            this._el.setAttribute("required", "required");
            this._el.setAttribute('placeholder', "Past diseases of the cardiovascular system");
            return this._el;
        }
    }
}

class Button {
    constructor(typeOfButton) {
        this._el = document.createElement('button');
        if (typeOfButton === "createVisitModalBtn") {
            this._el.setAttribute('type', 'submit');
            this._el.innerText = "CREATE";
            this._el.classList.add('create-visit-modal-btn');
            return this._el;
        }

        if (typeOfButton === "editVisitModalBtn") {
            this._el.setAttribute('type', 'submit');
            this._el.innerText = "EDIT";
            this._el.classList.add('edit-visit-modal-btn');
            return this._el;
        }
    }
}

class InputFieldTitle {
    constructor(fieldName) {
        this._el = document.createElement("span");
        this._el.classList.add("field-name");
        this._el.innerText = fieldName;
        return this._el;
    }
}
