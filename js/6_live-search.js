import Visit from "./4_visits.js";
import Request from "./1_ajax-requests.js";


export default class LiveSearch {
    constructor() {
        this.searchContainer = document.querySelector('.filter__container');
        this._getVisitsData();
        this._createSearchInput();
        this._createUrgencySelect();
    }

    static filterHide(){
        let filter = document.querySelector('.filter');
        filter.classList.add('hidden');
    }
    static filterShow(){
        let filter = document.querySelector('.filter');
        filter.classList.remove('hidden');
        new LiveSearch();
    }

    _createSearchInput() {
        this._input = document.createElement('input');
        this._input.classList.add('filter__live-search');
        this._input.type = 'text';
        this._input.placeholder = 'search by (id, full name, doctor)';
        this.searchContainer.append(this._input);
    }

    _createUrgencySelect() {
        const urgency = ['All', 'Regular', 'Priority', 'Urgent'];
        this._urgencySelect = document.createElement('select');
        const urgencyTitle = document.createElement('span');
        urgencyTitle.classList.add('filter__urgency-title');
        urgencyTitle.innerText = 'Select visit urgency:';
        this._urgencySelect.classList.add('filter__select-urgency');
        urgency.forEach(el => {
            this._selectOption = document.createElement('option');
            this._selectOption.text = el;
            this._urgencySelect.options.add(this._selectOption);
        });
        this.searchContainer.append(urgencyTitle);
        this.searchContainer.append(this._urgencySelect);

    }

    _getVisitsData() {
        new Promise((resolve, reject) => {
            resolve(new Request('getAllVisits'))
        })
            .then(data => {
                this._liveSearch(data);
                this._filterByUrgency(data);
            })
    }

    _liveSearch(allVisits) {
        this._input.addEventListener('input', function () {
            let visitsArray = [];
            let visitsContainer = document.querySelector(".visits");
            visitsContainer.innerHTML = "No data found";
            let inputedValue = this.value.trim();
            let inputedData = new RegExp(inputedValue, 'i');
            if (inputedValue !== '') {
                allVisits.forEach((el) => {
                    let inputedId = el.id.search(inputedData);
                    let inputedName = el.fullName.search(inputedData);
                    let inputedDoctor = el.doctor.search(inputedData);
                    if (inputedId !== -1 || inputedName !== -1 || inputedDoctor !== -1) {
                        visitsArray.push(el);
                    }
                });
                Visit.renderAllVisits(visitsArray);
            } else {
                Visit.renderAllVisits(allVisits);
            }
        });
    }

    _filterByUrgency(allVisits) {
        this._urgencySelect.addEventListener('change', (e) => {
            this._input.value = '';
            e.preventDefault();
            e.stopPropagation();
            let filteredUrgencyArray = [];
            let visitsContainer = document.querySelector(".visits");
            visitsContainer.innerHTML = "";
            if (e.currentTarget.value === 'All') {
                Visit.renderAllVisits(allVisits);
                this._liveSearch(allVisits);
            } else {
                allVisits.forEach(el => {
                    if (el.urgency === e.currentTarget.value) {
                        filteredUrgencyArray.push(el);
                    }
                });
                this._liveSearch(filteredUrgencyArray);
                Visit.renderAllVisits(filteredUrgencyArray);
            }
        });
    }
}